var gulp         = require('gulp'),
	sass         = require('gulp-sass'),
	postcss 	 = require('gulp-postcss'),
	autoprefixer = require('autoprefixer'),
	cleanCSS     = require('gulp-clean-css'),
	rename       = require('gulp-rename'),
	browserSync  = require('browser-sync').create(),
	concat       = require('gulp-concat'),
	uglify       = require('gulp-uglify');

gulp.task('browser-sync', ['styles', 'scripts'], function() {
		browserSync.init({
				server: {
						baseDir: "./app"
				},
				notify: false
		});
});

gulp.task('styles', function () {
	return gulp.src('./scss/style.scss')
	.pipe(sass().on('error', sass.logError))
	.pipe(postcss([ autoprefixer({browsers: [
		"> 1%",
		"last 40 versions",
		"ie 10",
		"ie 11",
		"IOS 7",
		"Firefox > 20",
		"Opera 12.1",
		"Safari 5.1"
	]}) ]))
	//.pipe(rename({suffix: '.min', prefix : ''}))
	//.pipe(cleanCSS())
	.pipe(gulp.dest('./app/css'))
	.pipe(browserSync.stream());
});

gulp.task('scripts', function() {
	return gulp.src([
		'./app/js/components/jquery-3.0.0.min.js',
		'./app/js/components/jquery-migrate-1.4.1.min.js',
		//'./app/js/components/jquery.formstyler.js',
		//'./app/js/components/jquery.fancybox.js',
		//'./app/js/components/jquery.mCustomScrollbar.js',
		//'./app/js/components/slick.js'
		])
		//.pipe(rename({suffix: '.min', prefix : ''}))
		.pipe(concat('components.js'))
			//.pipe(uglify()) //Minify libs.js
		.pipe(gulp.dest('./app/js/'));
});

gulp.task('watch', function () {
	gulp.watch('./scss/**/*.scss', ['styles']);
	gulp.watch('./app/components/**/*.js', ['scripts']);
	gulp.watch('./app/js/*.js').on("change", browserSync.reload);
	gulp.watch('./app/*.html').on('change', browserSync.reload);
});

gulp.task('default', ['browser-sync', 'watch']);
