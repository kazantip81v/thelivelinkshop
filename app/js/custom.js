$(document).ready(function(){

	/* placeholder*/	   
	$('input, textarea').each(function(){
 		var placeholder = $(this).attr('placeholder');
 		$(this).focus(function(){ $(this).attr('placeholder', '');});
 		$(this).focusout(function(){			 
 			$(this).attr('placeholder', placeholder);  			
 		});
 	});
	/* placeholder*/

	/* support-button animation*/
	$('.support-button').hover(
		function() {
			$(this).removeClass('anim-blink');
		},
		function(){
			$(this).addClass('anim-blink');
		});
	/* support-button animation*/

	/* add & remove class active - toggle-box__item*/
	$('.box-promo .toogle__item').click(function(){
		$('.toogle__item').removeClass('active');
		$(this).addClass('active');
	});
	/* add & remove class active - toggle-box__item*/

	/* tabs */
	$('.js-tabs').click(function(){
		var idWrap = $(this).attr('data-for');
		$('.js-wrap').addClass('hidden');
		$('#' + idWrap).removeClass('hidden');

		$('.toggle__item').removeClass('active');
		$(this).parent().addClass('active');
	});

	$('.js-btn-tabs').click(function(){
		var idWrap = $(this).attr('data-for');
		var nthChild = $(this).attr('data-child');

		$('.js-wrap').addClass('hidden');
		$('#' + idWrap).removeClass('hidden');

		$(this).parent().removeClass('active');
		$('.toggle__item:nth-child(' + nthChild  + ')').addClass('active');
	});
	/* tabs */

});

/* tabs slider*/
	var slider = function(elemId, leftBtn, rightBtn) {
	//constructor
		this.elem = document.getElementById(elemId);
		this.lBtn = document.getElementById(leftBtn);
		this.rBtn = document.getElementById(rightBtn);
		this.increment = 225;
		this.box = 2250;
		this.margin = 0;
		this.elem.style.marginLeft = "0";
	};

	slider.prototype.right = function() {
		//right move
		if (this.margin > -this.box/2) {
			this.margin = this.margin - this.increment;
			this.elem.style.marginLeft = this.margin + "px";
		} else {
			this.margin = this.margin;
		};
		//control over arrows classes
		if (this.margin !== 0) {
			this.lBtn.classList.add("active");
		} else {
			this.lBtn.classList.remove("active");
		};
		if (this.margin === -this.box/2) {
			this.rBtn.classList.remove("active");
		} else {
			this.rBtn.classList.add("active");
		};
	};

	slider.prototype.left = function() {
		//left move
		if (this.margin < 0) {
			this.margin = this.margin + this.increment;
			this.elem.style.marginLeft = this.margin + "px";
		} else {
			this.margin = this.margin;
		};
		//control over arrows classes
		if (this.margin !== 0) {
			this.lBtn.classList.add("active");
		} else {
			this.lBtn.classList.remove("active");
		};
		if (this.margin === -this.box/2) {
			this.rBtn.classList.remove("active");
		} else {
			this.rBtn.classList.add("active");
		};
	};

	var specialOffers = new slider("specialOffers", "sOLeft", "sORight");
	var bestSellers = new slider("bestSellers", "bsLeft", "bsRight");
	var newProduct = new slider("newProduct", "npLeft", "npRight");
	var soonSale = new slider("soonSale", "ssLeft", "ssRight");
/* tabs slider*/


/* viewport width */
function viewport(){
	var e = window, 
		a = 'inner';
	if ( !( 'innerWidth' in window ) )
	{
		a = 'client';
		e = document.documentElement || document.body;
	}
	return { width : e[ a+'Width' ] , height : e[ a+'Height' ] }
};
/* viewport width */
var handler = function(){

	if( /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent) ) {
		$('body').addClass('ios');
	} else{
		$('body').addClass('web');
	};

	$('body').removeClass('loaded'); 
	
	var dropMenu = document.querySelectorAll('.drop-menu');
		dropMenu.forEach(function(el){
			el.style = "";
		});
	$('#js-menu-btn').removeClass('active');

	
	var viewport_wid = viewport().width;
	var viewport_height = viewport().height;

	if(viewport_wid <= 554) {
		$('.js-change-content').each(function(){
			$(this).html('<span>Больше </span><span> &#10142;</span>');
		});
	} else {
		$('.js-change-content').each(function(index, elem){
			if(index <= 3) {$(this).html('<span>Другие статьи и обзоры </span><span> &#10142;</span>')};
			if(index === 4) {$(this).html('<span>Все производители </span><span> &#10142;</span>')};
			if(index === 5) {$(this).html('<span>Другие статьи и обзоры </span><span> &#10142;</span>')};
		});
	};

	var responsivSlaid = function(elemId, item, obj){
		var element,
			widthItem,
			marginItem,
			countSlide;

		element = $('#' + elemId);
		widthItem = Math.round(element.parent().width()/item);
		marginItem = (widthItem - 225)/2;
		countSlide = element.find('.product-box').length;

		if(item === 4){ countSlide *=1.2 };
		if(item === 3){ countSlide *=1.4 };
		if(item === 2){ countSlide *=1.6 };
		if(item === 1){ countSlide *=1.6 };

		element.find('.product-box').css({marginLeft: marginItem, marginRight: marginItem});
		element.css('width', widthItem * countSlide);
		
		obj.increment = widthItem;
		obj.box = widthItem * countSlide;
	};

	if(viewport_wid >= 1200){
		$('.product-box').css({marginLeft: 0, marginRight: 0});
	};

	if(viewport_wid >= 981 && viewport_wid <=1199){
		responsivSlaid('specialOffers', 4, specialOffers);
		responsivSlaid('bestSellers', 4, bestSellers);
		responsivSlaid('newProduct', 4, newProduct);
		responsivSlaid('soonSale', 4, soonSale);
	};
	if(viewport_wid >= 768 && viewport_wid <=980){
		responsivSlaid('specialOffers', 3, specialOffers);
		responsivSlaid('bestSellers', 3, bestSellers);
		responsivSlaid('newProduct', 3, newProduct);
		responsivSlaid('soonSale', 3, soonSale);
	};
	if(viewport_wid >= 501 && viewport_wid <=767){
		responsivSlaid('specialOffers', 2, specialOffers);
		responsivSlaid('bestSellers', 2, bestSellers);
		responsivSlaid('newProduct', 2, newProduct);
		responsivSlaid('soonSale', 2, soonSale);
	};
	if(viewport_wid <= 500){
		responsivSlaid('specialOffers', 1, specialOffers);
		responsivSlaid('bestSellers', 1, bestSellers);
		responsivSlaid('newProduct', 1, newProduct);
		responsivSlaid('soonSale', 1, soonSale);
	};

};

$(window).bind('load', handler);
$(window).bind('resize', handler);



