﻿var app = angular.module('MyApp', []);

app.controller('MyAppCtrl', function ($scope, $window, $http, $document) {

	$http({method: 'GET', url: 'products.json'})
	.then( 
		function success (response) {
			$scope.products = response.data;
		},
		function error (response) {
			console.log('Eroor data!');
		});

	$scope.url_menu = '/template/_menu.html';
	$scope.url_specialOffers = '/template/_special-offers.html';
	$scope.url_bestSellers = '/template/_best-sellers.html';
	$scope.url_newProduct = '/template/_new-product.html';
	$scope.url_soonSale = '/template/_soon-sale.html';


	$scope.menu_btn = function(){
		$scope.option =  $('#menu-select option:selected');
		$scope.dropMenuShow = '#drop-menu_' + $scope.option.val();

		$($scope.dropMenuShow).slideToggle('fast');
		$('#js-menu-btn').toggleClass('active');
	};
});